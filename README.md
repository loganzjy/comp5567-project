# comp5567-project


# Environment Requirement
Python pip
$ python -m pip install --upgrade pip

gRpc
$ python -m pip install grpcio
or System wide
$ sudo python -m pip install grpcio

gRPC tools 
$ python -m pip install grpcio-tools

# Generate gRPC code
in project root path
$ python -m grpc_tools.protoc -I./proto --python_out=. --grpc_python_out=. ./proto/blockchain.proto
