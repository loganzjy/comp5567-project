import hashlib
import random


# This file used to define the blockchain structure

# A block must include hash code (address), previous block's hash code, and the transaction context
class Block:
    def __init__(self):
        self.Hash = ''
        self.Transaction = ''
        self.PrevBlockHash = ''


# blockchain class is a list to append the block one by one.
class Blockchain:
    def __init__(self):
        self.blocks = []
        # when a new blockchain class was implement, the genesis block will be created as first block
        genesis_block = NewBlock('Genesis Block', '')
        self.blocks.append(genesis_block)
        self.target_hash = 0


# function of create a new block
def NewBlock(transaction: str, prevBlockHash: str):
    # new block hash (address) generation should include the previous block's hash
    hash_code = transaction + prevBlockHash
    hash = hashlib.sha1(hash_code.encode("utf-8")).hexdigest()
    # declare a block with Block class
    block = Block()
    block.Hash = hash
    block.Transaction = transaction
    block.PrevBlockHash = prevBlockHash
    return block


# function of append a new block with the blockchain
def AddBlock(blockchain: Blockchain, transaction: str, expect_hash: int):
    # if the miner give the correct puzzle answer (target_hash), then the miner's transaction will be accepted and added at new block
    if expect_hash == blockchain.target_hash:
        prevBlockHash = blockchain.blocks[-1].Hash
        block = NewBlock(transaction=transaction, prevBlockHash=prevBlockHash)
        blockchain.blocks.append(block)
        # Describe the status at terminal
        print("Blockchain updated: ")
        for i in range(len(blockchain.blocks)):
            print("block " + str(i) + ":")
            print('block hash = ' + blockchain.blocks[i].Hash)
            print('block transaction = ' + blockchain.blocks[i].Transaction)
        print()
        blockchain.target_hash = random.randint(0, 9)
        print("Next Block's Puzzle Hash is " + str(blockchain.target_hash))
        return blockchain.blocks[-1].Hash
    # if the miner give the wrong puzzle answer, return false.
    else:
        return "False"


# Encapsulate the function of creating a blockchain into NewBlockchain
def NewBlockchain():
    new_blockchain = Blockchain()
    return new_blockchain
