from concurrent import futures
import logging
import grpc
import blockchain_pb2
import blockchain_pb2_grpc
from bc_definition import *


# We implement the function at server class
class BlockchainServer(blockchain_pb2_grpc.BlockChainServicer):
    # When the server created, a new blockchain will be created too
    def __init__(self):
        self.blockchain = NewBlockchain()

    # Receive miner's transaction and expected hash. And return the result or the address of the new block to the miner.
    def AddBlock(self, request, context):
        hash_code = AddBlock(self.blockchain, request.transaction, request.expectHash)
        return blockchain_pb2.AddBlockResponse(hash=hash_code)

    # Receive the miner's query request and return the complete blockchain (list format)
    def QueryBlockchain(self, request, context):
        response = blockchain_pb2.QueryBlockchainResponse()
        # Question: What does this for loop statement do? How is his data structure transformed?
        for block in self.blockchain.blocks:
            pb2_block = blockchain_pb2.Block(transaction=block.Transaction, hash=block.Hash,
                                             prevBlockHash=block.PrevBlockHash)
            response.blocks.append(pb2_block)
        return response

    # For step 3
    def QueryBlock(self, request, context):
        response = blockchain_pb2.QueryBlockResponse()
        # TODO
        block = self.blockchain.blocks[-1]
        Block = blockchain_pb2.Block(transaction=block.Transaction, hash=block.Hash,prevBlockHash=block.PrevBlockHash)
        return blockchain_pb2.QueryBlockResponse(block=Block)


# server setting
def serve():
    port = '50051'
    server = grpc.server(futures.ThreadPoolExecutor(max_workers=10))
    blockchain_pb2_grpc.add_BlockChainServicer_to_server(BlockchainServer(), server)
    server.add_insecure_port('127.0.0.1:' + port)
    server.start()
    print("blockchain-demo started, listening on " + port)
    server.wait_for_termination()


# run the server
if __name__ == '__main__':
    logging.basicConfig()
    serve()
