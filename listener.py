from __future__ import print_function

import logging

import grpc
import blockchain_pb2
import blockchain_pb2_grpc

# The client request the QueryBlock to server, transferring the message by Block data structure
def run():
    with grpc.insecure_channel('localhost:50051') as channel:
        stub = blockchain_pb2_grpc.BlockChainStub(channel)
        # TODO
        while 1 == 1:
            time.sleep(1)
            block = stub.QueryBlock(blockchain_pb2.QueryBlockRequest(message=''))
            print("Received latest Block: ")
            print(block)


if __name__ == '__main__':
    logging.basicConfig()
    run()
